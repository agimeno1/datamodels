# References

- [Smart Data Model Search Tool](https://smartdatamodels.org/index.php/ddbb-of-properties-descriptions/)
- [Forbidden chars in entity attributes](https://fiware-orion.readthedocs.io/en/3.6.0/user/forbidden_characters/)
- [Unit codes](https://unece.org/sites/default/files/2021-06/rec20_Rev17e-2021.xlsx)